<html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<head>
<title>Networks</title>
<script>
function openForm() {
  document.getElementById("myForm").style.display = "block";
}

function closeForm() {
  document.getElementById("myForm").style.display = "none";
}

function doSomething() {

	alert("Registered Successfully");

}
</script>

<script type="text/javascript">
	function renewal()
	{
		
		alert("Registered Successfully")
	}
</script>
<style>

.accordion {
  font-family: Hack;
  background-color: #eee;
  color: #444;
  cursor: pointer;
  padding: 12px;
  width: 17%;
  margin: 4px 2px;
  text-align: center;
  outline: none;
  font-size: 20px;
  font-weight: bold;
  transition: 0.4s;
  display: table-row;
  border: 2px solid #001a00;
}

.active, .accordion:hover {
  background-color: #3366ff;
  width: 20%;
}

.panel {
  padding: 0 17px;
  display: none;
  width: 24%;
  background-color: white;
  overflow: hidden;
}

.navbar {

  width: 100%;
  background-color: #555;
  overflow: auto;
}

.navbar a 
{
	font-family: Hack;
	float: left;
	padding: 12px;
	color: white;
	text-decoration: none;
	font-size: 17px;
	width: 25%; /* Four links of equal widths */
	text-align: center;
}

.navbar a:hover {
  background-color: #000;
}

.active {
  background-color: blue;
}
@media screen and (max-width: 500px) {
  .navbar a {
    float: none;
    display: block;
  }
}
* {
  box-sizing: border-box;
}

.img-area {
	background-image: url('book1.jpg');
	-webkit-background-size: cover;
	background-size: cover;
	background-position: all;
	height: 100%;
	position: fixed;
	left: 0;
	right: 0;
	z-index: -1;
	filter: blur(6px);
}


/* Create two unequal columns that floats next to each other */
.column {
  float: left;
  padding: 10px;
  height: 300px; /* Should be removed. Only for demonstration */
}

.left {
  width: 25%;
  height: 550px;
}

.right {
  width: 75%;
  height: 550px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

.button {
  background-color: #008CBA;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  font-family: Hack;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 12px;
  padding: 12px 40px;
  box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24), 0 17px 50px 0 rgba(0,0,0,0.19);
  transition-duration: 0.4s;
}

.button:active {
  background-color: #3e8e41;
  box-shadow: 0 5px #666;
  transform: translateY(4px);
}

.display {
	font-size: 23px;
	font-family: Hack;
}
/** {box-sizing: border-box;}*/

/* Button used to open the contact form - fixed at the bottom of the page */
.open-button {
  background-color: #555;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  opacity: 0.8;
  position: fixed;
  bottom: 23px;
  right: 28px;
  width: 280px;
}

/* The popup form - hidden by default */
.form-popup {
  display: none;
  position: fixed;
  bottom: 0;
  right: 25px;
  border: 3px solid #f1f1f1;
  z-index: 9;
}

/* Add styles to the form container */
.form-container {
  max-width: 300px;
  padding: 10px;
  background-color: white;
}

/* Full-width input fields */
.form-container input[type=text], .form-container input[type=password] {
  width: 100%;
  padding: 15px;
  margin: 5px 0 10px 0;
  border: none;
  background: #f1f1f1;
}

/* When the inputs get focus, do something */
.form-container input[type=text]:focus, .form-container input[type=password]:focus {
  background-color: #ddd;
  outline: black;
}

/* Set a style for the submit/login button */
.form-container .btn {
  background-color: #4CAF50;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  margin-bottom:10px;
  opacity: 0.8;
}

/* Add a red background color to the cancel button */
.form-container .cancel {
  background-color: red;
}

/* Add some hover effects to buttons */
.form-container .btn:hover, .open-button:hover {
  opacity: 1;
}

.botface 
{
	font-family: Hack;
	font-size: 25px;
	position: none;
}
</style>

</head>

<body>
	<br>
<div class="img-area"></div>
<div class="navbar">
  <a class="active" href="#"><i class="fa fa-fw fa-home"></i> Home</a> 
  <a href="#"><i class="fa fa-fw fa-search"></i> Search</a> 
  <a href="#"><i class="fa fa-fw fa-envelope"></i> Contact</a> 
  <a href="#"><i class="fa fa-fw fa-user"></i> Aboutus</a>
</div>
	<h1 style="font-size: 30px; font-family: Hack; text-align: center;">Advanced Networks</h1>

	<div class="row">
	  	<div class="column left">

		    <img src="/images/networks.jpg" alt="Advanced Networks Book" width="400" height="550" class="center">
	  
	  	</div>

	  	<div class="column right" style="font-size: 25px; font-family: Hack; text-align: justify; padding-left: 120px">
	  		
	  		<b style="font-family: Hack; font-size: 25px;">Description</b>
	    	
	    	<p>This Book gives a deep and in-depth knowledge on how communication takes place between two parties and how they are established. It also covers SDN's which is used in most Private sector's. Even in Interview people test your knowledge on SDN's and for those who are looking forward to gain a foothold on Networking field this book is a great start.<br> <br> So who are Eligable to avail this book?. Students who are pursuing B.Tech / M.Tech in (Information Technology) can avail this book.</p>

	      <?php
	        $user = "";
	        $password = '';
	        $db = '';
	        $host = 'localhost';
	        $conn = new mysqli($host, $user, $password, $db);
	        if ($conn->connect_error) 
	        {
	          die("Connection failed: " . $conn->connect_error);
	        }
	        else 
	        {

	          $sql = "SELECT available FROM networks WHERE id = 1";

	          $total = $conn->query($sql);

	          while($row = mysqli_fetch_array($total)) 
	          {

	            echo "<strong>Books Available: </strong>" . "<td>" . $row[available] . "</td>";
	          }
	        }

	        if(array_key_exists('button1', $_POST)) 
	        { 
	      		button1(); 
	   		} 
	    	else if(array_key_exists('button2', $_POST)) 
	    	{ 
	      		button2(); 
	    	} 
		    function button1() 
		    { 
		    	$user = "";
	        	$password = '';
	        	$db = '';
	        	$host = 'localhost';
	        	$conn = new mysqli($host, $user, $password, $db);
	        	if ($conn->connect_error) 
	        	{
	          		die("Connection failed: " . $conn->connect_error);
	        	}
	        	else 
	        	{
	        		$count = "UPDATE networks SET available = available - 1 WHERE id = 1";

	        		if ($conn->query($count) == true) 
		    		{
		      			echo '<script type="text/javascript">',
	     					 'doSomething();',
	     					 '</script>'
							 ;
		      		}
	        	}	
		    }  

	      ?>
		<form method="post">
	    	<div class="form-popup" id="myForm">
	  			<div class="form-container">
		    		<h1>Renewal</h1>

				    <label for="bnum"><b>Book-number</b></label>
				    <input type="text" id="booknum" placeholder="Enter Book.no " required name="roll" >

				    <label for="rnum"><b>Roll-number</b></label>
				    <input type="text" placeholder="Enter Roll.no" name="psw" required>

				    <label for="mnum"><b>Mobile-number</b></label>
				    <input type="text" placeholder="Enter Mob.no" name="psw" required>

				    <button type="submit" id="renew" class="btn" onclick="renewal();" >Renew
				    </button>
				    <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
			  </div>
			</div>
		</form>
	      	<form method="post">
			    <div class="row">
			    	<div class="column right">
				    		<button id="btn3" class="button" name="button1" type="submit" style="font-size: 25px; text-align: center; margin-right: 60px; margin-left: -15px;">Register
				    		</button>
				    		
				    		<button class="button" type="button" onclick="openForm();" style="font-size: 25px; text-align: center;">Renew</button>
			    		</div>
					</div>
		    	</div>
		    </form>
	  	</div>
	</div><br><br>
<div class="botface" >
	<p><b>Preface</b></p>
	<p class="preface">It has been nearly ten years since the fifth edition of Computer Networks: A Systems Approach was published. Much has changed in that time, most notably, the explosion of the cloud and smartphone apps onto the scene. In many ways, this is reminiscent of the dramatic effect the Web was having on the Internet when we published the first edition of the book in 1996.</p><br>
</div>
<div class="botface">
	<b>Contents Of Chapter</b><br><br>

	<button class="accordion">Unit 1</button>
	<div class="panel">
		<b>
		<ol>
			<li>Overview of Java</li>
			<li>Object Oriented Concepts</li>
				<ol>
					<li> Classes and methods</li>
					<li>Inheritance</li>
					<li>Polymorphism</li>
					<li>Interfaces</li>
					<li>packages</li>
						<ol>
							<li>JAR files and Annotation</li>
						</ol>
				</ol>
			<li>I/O</li>
				<ol>
					<li>Files</li>
					<li>Streams</li>
					<li>Object Serialization</li>
				</ol>
			<li>Multithreading</li>
			<li>Networking</li>
			<li>Generic Collections</li>
			<li>Generic Classes and Methods</li>
		</ol>
	</b>
	</div>

	<button class="accordion">Unit 2</button>
	<div class="panel">
		<b>
			<ol>
				<li>Overview of HTML5</li>
				<li>Cascading Style Sheets</li>
				<li>Overview of JavaScript</li>
				<li>Events Handling</li>
				<li>Regular Expressions</li>
				<li>HTML DOM</li>
				<ol>
					<li>Web Browser BOM</li>
					<li>AJAX</li>
					<li>JSON</li>
				</ol>
				<li>Dynamic WebPages</li>
				<li>Jquery</li>
				<li>Overview of Angular JS</li>
			</ol>
		</b>
	</div>

	<button class="accordion">Unit 3</button>
	<div class="panel">
		<b>
			<ol>
				<li>Database Connectivity</li>
				<li>JDBC Drivers</li>
				<li>Servlets</li>
				<li>Servlet API</li>
				<li>Servlet Configuration</li>
				<li>Running Servlet with Database Connectivity</li>
				<li>Basics of JSP</li>
				<li>Java Server Faces</li>
				<li>MVC Architecture of JSF Apps</li>
				<li>JSF Components</li>
				<li>Session Tracking</li>
				<li>Accessing Databases in Web Apps</li>
				<li>Developing Dynamic Data Driven Websites</li>
			</ol>
		</b>
	  
	</div>

	<button class="accordion">Unit 4</button>
	<div class="panel">
		<b>
			<ol>
				<li>Distributed Objects</li>
				<li>RMI Programming Model</li>
				<li>Java Beans Component</li>
				<li>Java Beans API</li>
				<li>XML</li>
				<li>Java XML API</li>
				<li>XML</li>
				<li>RPC</li>
				<li>WSDL</li>
				<li>SOAP</li>
				<li>Overview of Java Web Services</li>
				<li>JAX-WS</li>
				<li>RESTful Web Services</li>
			</ol>
		</b>
	  
	</div>

	<button class="accordion">Unit 5</button>
	<div class="panel">
		<b>
			<ol>
				<li>Hibernate Architecture</li>
				<li>Overview of HQL</li>
				<li>O/R Mapping</li>
				<li>Working with Hibernate</li>
				<li>MVC Architecture</li>
				<li>Struts</li>
				<li>Understanding Actions</li>
				<li>Dependency Injection and Inversion of Control</li>
				<li>Spring 3.0</li>
				<li>Dependency Injection</li>
				<li>Spring Library</li>
				<li>Developing Applications</li>
				<li>Case Studies</li>
				<li>Current Trends</li>
			</ol>
		</b>
	  
	</div>
</div>

<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
</body>
</html>
