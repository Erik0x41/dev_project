<?php
$user = "admin";
	        $password = '';
	        $db = '';
	        $host = 'localhost';
	        $conn = new mysqli($host, $user, $password, $db);
	        if ($conn->connect_error) 
	        {
	          die("Connection failed: " . $conn->connect_error);
	        }
	        else 
	        {
	        	$username = $_POST['roll'];
	        	$password = $_POST['dob'];
	        	$query = "SELECT * FROM login WHERE username = '$username' AND password = '$password'";
	        	$result = mysqli_query($conn,$query);
	        	$count = mysqli_num_rows($result);
	        	if ($count > 0)
	        	{
	        		header("Location: http://localhost/books.html");
	        	}
	        	else
	        	{
	        		echo "Invalid Credentials.";
	        	}
	        }
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

body {font-family: Hack;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
  width: 30%;
  display: grid;
  place-items: center;
  padding: 12px 20px;
  margin: auto;
  border: 1px solid #ccc;
  box-sizing: border-box;
}

button {
  background-color: #4CAF50;
  font-family: Hack;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  cursor: pointer;
  width: 100%;
}

button:hover {
  opacity: 0.8;
}

.cancelbtn {
  width: auto;
  padding: 10px 18px;
  background-color: #f44336;
}

.imgcontainer {
  text-align: center;
  margin: 24px 0 12px 0;
}

img.avatar {
  width: 15%;
  border-radius: 10%;
}

.container {
  padding: 16px;
}

span.psw {
  float: right;
  padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
  span.psw {
     display: block;
     float: none;
  }
  .cancelbtn {
     width: 100%;
  }
}

button {
  width: 200px;
  font-size: 15px;
  border-radius: 10px;
  margin: 20px;
  margin-left: 500px;
}

.flex-parent {
  display: flex;
}

.jc-center {
  justify-content: center;
}
</style>
</head>
<body>

<h2 style="font-family: Hack; text-align: center;">Student Library Login</h2>

<form method="post">
  <div class="imgcontainer">
    <img src="/logo/aulogo.png" alt="Avatar" class="avatar">
  </div>

  <div class="container">
    <input type="text" style="font-family: Hack;" placeholder="Enter Roll.No" name="roll" required>
    <br>

    <input type="password" style="font-family: Hack;" placeholder="Enter Date Of Birth" name="dob" required>
  </div>

  <div class="flex-parent jc-center">
    <button type="submit" style="margin-left: 25px; ">Login</button>
    <a href="">
    <button type="button" style=" background-color: #ff3333; margin-left: -10px; ">Cancel</button>
  </div>


  <div class="container">
    <p>Forgot <a href="#">password</a></p>
  </div>
</form>

</body>
</html>
