# dev_project

My first custom website to show demo on webpages.

#NOTE: For security reasons I have removed the username and password for the database from the files.

#Languages used: HTML, CSS, PHP, JavaScript
#Backend DB: MySQL

Here I have build an Library for a college where the students can see whether the book needed for the student is available in the library or not. (just a concept)
If available they can register for the book and get it from the library.

here the website fetches values of the book available from the database, and displays on the webpage.
